#pragma once

#include "DBAccess.h"
#include "string"
#include "iostream"

std::list<Album> DBAccess::getAlbums()
{
	std::list<Album> albums;
	auto callback = [](void* data, int argc, char** argv, char** azColName)
	{
		static_cast<std::list<Album>*>(data)->push_back(Album(std::stoi(argv[2]), argv[1], argv[3]));
		return 0;
	};

	const std::string sqlCommand = "SELECT * FROM ALBUMS";
	char* errMessage = nullptr;
	const auto res = sqlite3_exec(_db, sqlCommand.c_str(), callback, &albums, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << errMessage << std::endl;
	}

	for (auto& album : albums)
	{
		for (auto& picture : getPicturesByAlbumName(album.getName()))
		{
			album.addPicture(picture);
		}
	}
	
	return albums;
}

std::list<Album> DBAccess::getAlbumsOfUser(const User& user)
{
	std::list<Album> albums;

	for (const auto album : getAlbums())
	{
		if (album.getOwnerId() == user.getId())
		{
			albums.push_back(album);
		}
	}

	return albums;
}

void DBAccess::createAlbum(const Album& album)
{
	const std::string sqlStatement = "INSERT INTO ALBUMS (NAME, USER_ID, CREATION_DATE) VALUES ('"
		+ album.getName()
		+ "', "
		+ std::to_string(album.getOwnerId())
		+ ", '"
		+ album.getCreationDate()
		+ "')";
	
	std::cout << sqlStatement << std::endl;
	char* errMessage = nullptr;
	const auto res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << errMessage << std::endl;
	}

}

void DBAccess::deleteAlbum(const std::string& albumName, const int userId)
{
	for (const auto& picture : openAlbum(albumName).getPictures())
	{
		removePictureFromAlbumByName(albumName, picture.getName());
	}
	
	const std::string sqlStatement = "DELETE FROM ALBUMS WHERE NAME = '" + albumName + "' AND USER_ID = " + std::to_string(userId);
	std::cout << sqlStatement << std::endl;
	char* errMessage = nullptr;
	const auto res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << errMessage << std::endl;
	}
}

bool DBAccess::doesAlbumExists(const std::string& albumName, int userId)
{
	std::string name = "null";
	return !getAlbumsOfUser(User(userId, name)).empty();
}

Album DBAccess::openAlbum(const std::string& albumName)
{
	for (auto album : getAlbums())
	{
		if (album.getName() == albumName)
			return album;
	}
	return Album();
}

void DBAccess::closeAlbum(Album& pAlbum)
{
}

void DBAccess::printAlbums()
{
	for (auto album : getAlbums())
	{
		std::cout
		<< "name: " << album.getName()
		<< " owner_id: " << album.getOwnerId()
		<< " date: " << album.getCreationDate()
		<< std::endl;
	}
}

std::list<Picture> DBAccess::getPicturesByAlbumName(const std::string& aName) const
{
	std::list<Picture> pictures;

	auto callback = [](void* data, int argc, char** argv, char** azColName)
	{
		static_cast<std::list<Picture>*>(data)->push_back(
			Picture(std::stoi(argv[0]), argv[1], argv[2], argv[3])
		);
		return 0;
	};

	std::string sqlCommand = "SELECT * FROM PICTURES WHERE ALBUM_ID = " + std::to_string(getAlbumId(aName));
	char* errMessage = nullptr;
	auto res = sqlite3_exec(_db, sqlCommand.c_str(), callback, &pictures, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << errMessage << std::endl;
	}


	for (auto& picture : pictures)
	{
		for (auto tag : getAllTags(picture.getId()))
		{
			picture.tagUser(tag);
		}
	}

	return pictures;
}

void DBAccess::addPictureToAlbumByName(const std::string& albumName, const Picture& picture)
{
	const int albumId = getAlbumId(albumName);

	const std::string sqlCommand = "INSERT INTO PICTURES (NAME, LOCATION, CREATION_DATE, ALBUM_ID) VALUES ('"
		+ picture.getName()
		+ "', '"
		+ picture.getPath()
		+ "', '"
		+ picture.getCreationDate()
		+ "', "
		+ std::to_string(albumId)
		+")";

	
	char* errMessage = nullptr;
	const auto res = sqlite3_exec(_db, sqlCommand.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << errMessage << std::endl;
	}
}

void DBAccess::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName)
{
	auto tags = getAllTags(getPictureId(pictureName));
	for (auto tag : tags)
	{
		untagUserInPicture(albumName, pictureName, tag);
	}
	
	const int albumId = getAlbumId(albumName);
	const std::string sqlCommand = "DELETE FROM PICTURES WHERE NAME = '" + pictureName + "'";
	char* errMessage = nullptr;
	const auto res = sqlite3_exec(_db, sqlCommand.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << errMessage << std::endl;
	}
}

auto DBAccess::tagUserInPicture(const std::string& albumName, const std::string& pictureName, const int userId) -> void
{
	const int pictureId = getPictureId(pictureName);
	const std::string sqlCommand = "INSERT INTO TAGS (PICTURE_ID, USER_ID) VALUES ("
		+ std::to_string(pictureId)
		+ ", "
		+ std::to_string(userId)
		+ ")";

	char* errMessage = nullptr;
	const auto res = sqlite3_exec(_db, sqlCommand.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << errMessage << std::endl;
	}
}

void DBAccess::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	const int pictureId = getPictureId(pictureName);

	const std::string sqlCommand = "DELETE FROM TAGS WHERE PICTURE_ID = " + std::to_string(pictureId)
		+ " AND USER_ID = " + std::to_string(userId);
	
	char* errMessage = nullptr;
	const auto res = sqlite3_exec(_db, sqlCommand.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << errMessage << std::endl;
	}
}

std::list<int> DBAccess::getAllTags(const int pictureId) const
{
	std::list<int> tags;

	auto callback = [](void* data, int argc, char** argv, char** azColName)
	{
		static_cast<std::list<int>*>(data)->push_back(std::stoi(argv[0]));
		return 0;
	};

	const std::string sqlCommand = "SELECT USER_ID FROM TAGS WHERE PICTURE_ID = " + std::to_string(pictureId);
	char* errMessage = nullptr;
	const auto res = sqlite3_exec(_db, sqlCommand.c_str(), callback, &tags, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << errMessage << std::endl;
	}

	return tags;
}

std::list<User> DBAccess::getUsers() const
{
	std::list<User> users;

	auto callback = [](void* data, int argc, char** argv, char** azColName)
	{
		static_cast<std::list<User>*>(data)->push_back(User(std::stoi(argv[0]), argv[1]));
		return 0;
	};

	const std::string sqlCommand = "SELECT * FROM USERS";
	char* errMessage = nullptr;
	const auto res = sqlite3_exec(_db, sqlCommand.c_str(), callback, &users, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << errMessage << std::endl;
	}

	return users;
}

void DBAccess::printUsers()
{
	for (const auto& user : getUsers())
	{
		std::cout << "id: " << user.getId() << " name: " << user.getName() << std::endl;
	}
}

void DBAccess::createUser(User& user)
{
	std::string sqlCommand = "INSERT INTO USERS (NAME) VALUES ('" + user.getName() + "')";
	char* errMessage = nullptr;
	const auto res = sqlite3_exec(_db, sqlCommand.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << errMessage << std::endl;
	}
}

void DBAccess::deleteUser(const User& user)
{
	auto userAlbums = getAlbumsOfUser(user);
	for (const auto& userAlbum : userAlbums)
	{
		deleteAlbum(userAlbum.getName(), user.getId());
	}

	deleteUserTags(user);

	const auto sqlCommand = "DELETE FROM USERS WHERE NAME = '" + user.getName() + "'";
	char* errMessage = nullptr;
	const auto res = sqlite3_exec(_db, sqlCommand.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << errMessage << std::endl;
	}
}

bool DBAccess::doesUserExists(int userId)
{
	for (const auto& user : getUsers())
	{
		if (user.getId() == userId) 
			return true;
	}
	return false;
}

User DBAccess::getUser(int userId)
{
	for (const auto user : getUsers())
	{
		if (user.getId() == userId)
			return user;
	}
	return User();
}

void DBAccess::deleteUserTags(const User& user) const
{
	const auto sqlCommand = "DELETE FROM TAGS WHERE USER_ID = " + std::to_string(user.getId());
	char* errMessage = nullptr;
	const auto res = sqlite3_exec(_db, sqlCommand.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << errMessage << std::endl;
	}
}

int DBAccess::countAlbumsOwnedOfUser(const User& user)
{
	int albumsCount = -1;
	auto callback = [](void* data, int argc, char** argv, char** azColName)
	{
		*static_cast<int*>(data) = std::stoi(argv[0]);
		return 0;
	};

	const std::string sqlCommand = "SELECT COUNT (NAME) FROM ALBUMS WHERE USER_ID = " + std::to_string(user.getId());
	char* errMessage = nullptr;
	const auto res = sqlite3_exec(_db, sqlCommand.c_str(), callback, &albumsCount, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << errMessage << std::endl;
	}

	return albumsCount;
}

int DBAccess::countAlbumsTaggedOfUser(const User& user)
{
	return 0;
}

int DBAccess::countTagsOfUser(const User& user)
{
	int tags = -1;
	auto callback = [](void* data, int argc, char** argv, char** azColName)
	{
		*static_cast<int*>(data) = std::stoi(argv[0]);
		return 0;
	};

	const std::string sqlCommand = "SELECT COUNT (PICTURE_ID) FROM TAGS WHERE USER_ID = " + std::to_string(user.getId());
	char* errMessage = nullptr;
	const auto res = sqlite3_exec(_db, sqlCommand.c_str(), callback, &tags, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << errMessage << std::endl;
	}

	return tags;
}

float DBAccess::averageTagsPerAlbumOfUser(const User& user)
{
	return 0;
}

User DBAccess::getTopTaggedUser()
{
	int maxTags = -1;
	User topUser;

	for (const auto& user : getUsers())
	{
		if (countTagsOfUser(user) > maxTags)
		{
			maxTags = countTagsOfUser(user);
			topUser = user;
		}
	}
	
	return topUser;
}

Picture DBAccess::getTopTaggedPicture()
{
	Picture topPicture;
	
	for (const auto& album : getAlbums())
	{
		for (const auto picture : album.getPictures())
		{
			if (picture.getTagsCount() > topPicture.getTagsCount())
			{
				topPicture = picture;
			}
		}
	}

	return topPicture;
}

std::list<Picture> DBAccess::getTaggedPicturesOfUser(const User& user)
{
	std::list<Picture> pictures;

	for (const auto& album : getAlbums())
	{
		for (const auto& picture : album.getPictures())
		{
			for (auto tag : picture.getUserTags())
			{
				if (user.getId() == tag)
					pictures.push_back(picture);
			}
		}
	}
	
	return pictures;
}

bool DBAccess::open()
{
	const auto res = sqlite3_open(_dbFilename.c_str(), &_db);
	if (res == SQLITE_OK)
	{
		std::cout << "[+] Database opened successfully" << std::endl;
	}
	return res == SQLITE_OK;
}

void DBAccess::close()
{
	sqlite3_close(_db);
	_db = nullptr;
}

void DBAccess::clear()
{
}

auto DBAccess::getPictureId(const std::string& pName) const -> int
{
	int pictureId = -1;
	auto callback = [](void* data, int argc, char** argv, char** azColName)
	{
		*static_cast<int*>(data) = std::stoi(argv[0]);
		return 0;
	};

	const std::string sqlCommand = "SELECT ID FROM PICTURES WHERE NAME = '" + pName + "'";
	char* errMessage = nullptr;
	const auto res = sqlite3_exec(_db, sqlCommand.c_str(), callback, &pictureId, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << errMessage << std::endl;
	}

	return pictureId;
}

auto DBAccess::getAlbumId(const std::string& aName) const -> int
{
	int albumId = -1;
	auto callback = [](void* data, int argc, char** argv, char** azColName)
	{
		*static_cast<int*>(data) = std::stoi(argv[0]);
		return 0;
	};

	const std::string sqlCommand = "SELECT ID FROM ALBUMS WHERE NAME = '" + aName + "'";
	char* errMessage = nullptr;
	const auto res = sqlite3_exec(_db, sqlCommand.c_str(), callback, &albumId, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << errMessage << std::endl;
	}

	return albumId;
}
